﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ClosestTarget : MonoBehaviour
{
    private GameObject[] multipleobjects;
    public Transform closestobject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        closestobject = null;
         closestobject = getClosestenemy();
        transform.position = closestobject.position;
        
        transform.position = closestobject.position;
       Debug.Log("nearestTarget= " + closestobject.position);
    }

    public Transform getClosestenemy()
    {
        multipleobjects = GameObject.FindGameObjectsWithTag("tile");
        float closestdistance = Mathf.Infinity;
        Transform trans = null;
        
        foreach(GameObject go in multipleobjects)
        {
            float currentdistance;
            currentdistance = Vector3.Distance(transform.position, go.transform.position);
            if(currentdistance < closestdistance)
            {
                closestdistance = currentdistance;
                trans = go.transform;
    
            }

        }
        return trans;
    }




}
